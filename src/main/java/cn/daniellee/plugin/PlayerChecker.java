package cn.daniellee.plugin;

import cn.daniellee.plugin.component.LoginChecker;
import cn.daniellee.plugin.component.LoginCounter;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

public class PlayerChecker extends JavaPlugin {

	public static FileConfiguration config;

	public static Logger logger;

	public static int countInterval; //连接次数统计间隔（秒）

	public static double limitPerSecond; //开启防御的每秒连接阈值

	public static int secondCount = 0;

	public static boolean enableDefense = false;

	public static boolean toggleDefense = false;

	public static File listFile;

	private static LoginCounter loginCounter = null;

	public void onEnable(){
		logger = getLogger();

		loadConfig();

		getLogger().info(" ");
		getLogger().info(">>>>>>>>>>>>>>>>>>>>>>>> PlayerChecker Loaded <<<<<<<<<<<<<<<<<<<<<<<<");
		getLogger().info(">>>>> If you encounter any bugs, please contact author's QQ: 768318841 <<<<<");
		getLogger().info(" ");

		Bukkit.getPluginCommand("playerchecker").setExecutor(this);

		Bukkit.getPluginManager().registerEvents(new LoginChecker(), this);
	}

	private void loadConfig() {
		getLogger().info("[PlayerChecker] Loading config...");
		config = getConfig();
		countInterval = config.getInt("count-inerval", 10);
		limitPerSecond = config.getDouble("limit-per-second", 3D);
		if(!getDataFolder().exists()) getDataFolder().mkdirs();
		//加载玩家白名单列表
		listFile = new File(getDataFolder(), "players.txt");
		if (!listFile.exists()) {
			try {
				listFile.createNewFile();
			} catch (IOException e) {
				getLogger().info("[PlayerChecker]" + config.getString("create-error-log", "White list file create error.") + e.getMessage());
			}
		}
		if (loginCounter != null) loginCounter.cancel();
		loginCounter = new LoginCounter();
		loginCounter.runTaskTimer(this, 0L, 20 * countInterval);
		saveDefaultConfig();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length > 0 && sender.isOp()) {
			if ("reload".equalsIgnoreCase(args[0])) {
				reloadConfig();
				loadConfig();
				sender.sendMessage("[PlayerChecker]" + config.getString("reload-success", "Reload success."));
			} else if ("toggle".equalsIgnoreCase(args[0])) {
				toggleDefense = !toggleDefense;
				sender.sendMessage("[PlayerChecker]" + config.getString("intercept-manual", "Open the intercept by manual: ") + (toggleDefense ? config.getString("enable-mark", "Enabled") : config.getString("disable-mark", "Disabled")));
			} else sender.sendMessage("[PlayerChecker]" + config.getString("invalid-command", "Invalid command."));
		} else sender.sendMessage("[PlayerChecker]" + config.getString("no-permission", "You don't have permission."));
		return true;
	}

	@Override
	public void onDisable() {
		getLogger().info(" ");
		getLogger().info(">>>>>>>>>>>>>>>>>>>>>>>> PlayerChecker Unloaded <<<<<<<<<<<<<<<<<<<<<<<<");
		getLogger().info(" ");
	}

}