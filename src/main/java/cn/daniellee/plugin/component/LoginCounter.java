package cn.daniellee.plugin.component;

import cn.daniellee.plugin.PlayerChecker;
import org.bukkit.scheduler.BukkitRunnable;

import java.math.BigDecimal;
import java.util.Objects;

public class LoginCounter extends BukkitRunnable {

	@Override
	public void run() {
		if (!PlayerChecker.toggleDefense) {
			double perSecond = new BigDecimal(PlayerChecker.secondCount).divide(new BigDecimal(PlayerChecker.countInterval), 1, BigDecimal.ROUND_HALF_UP).doubleValue();
			PlayerChecker.enableDefense = perSecond > PlayerChecker.limitPerSecond;
			PlayerChecker.logger.info(Objects.requireNonNull(PlayerChecker.config.getString("frequency-log", "Current connect frequency: %times% times/second, defense status: %status%")).replace("%times%", String.valueOf(perSecond)).replace("%status%", Objects.requireNonNull(PlayerChecker.enableDefense ? PlayerChecker.config.getString("enable-mark", "Enabled") : PlayerChecker.config.getString("disable-mark", "Disabled"))));
			PlayerChecker.secondCount = 0;
		}
	}

}
